<?php

namespace UpdaterBundle\StartUp;

use Guzzle\Http\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UpdaterBundle\Manager\SystemInformationManager;

class PowerOnCommand extends StartUpCommand
{
    protected function configure()
    {
        $this
            ->setName('updater:poweron')
            ->setDescription('This function should be called after boot. It sends the basic machine info and network info to the CMS/CDS')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $this->getHostServer();
        $systemInfo = new SystemInformationManager();      
        
        $sysInfo['cpucores'] = $systemInfo->getNumberOfCore();
        $sysInfo['cputype']  = $systemInfo->getCpuType();
        $sysInfo['graphics'] = $systemInfo->getGraphics();
        $sysInfo['hdserial'] = $systemInfo->getHdSerial();
        $sysInfo['hdtype']   = $systemInfo->getHdModel();
        $sysInfo['mbtype']   = $systemInfo->getMotherBoardModel();
        $sysInfo['memory']   = $systemInfo->getMemorySize();
        $sysInfo['os']       = 'Windows';
        $json = json_encode($sysInfo);

        $query['box']      = $systemInfo->getMacAddress();
        $query['hash']     = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);
        $query['csq']      = $systemInfo->getCSQ();
        $query['jsoninfo'] = base64_encode($json);

        $client = new Client();
        $response = $client->get('https://'.$host.'/updater/poweron.php', [], [
            'query' => $query
        ]);
        $response->send();
    }
}