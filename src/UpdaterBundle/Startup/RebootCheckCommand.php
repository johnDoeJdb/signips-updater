<?php

namespace UpdaterBundle\StartUp;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UpdaterBundle\Response\Command\AbstractCommand as Notifier;

class RebootCheckCommand extends StartUpCommand
{
    protected function configure()
    {
        $this
            ->setName('updater:reboot:check')
            ->setDescription('Check if it was rebooted by command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (file_exists('D:\\temporary\\reboot')) {
            $updaterId = file_get_contents('D:\\temporary\\reboot');
            exec('del D:\\temporary\\reboot');
            Notifier::done($updaterId, 'Reboot successful');
        }
    }
}