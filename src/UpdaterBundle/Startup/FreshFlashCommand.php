<?php

namespace UpdaterBundle\StartUp;

use Guzzle\Http\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UpdaterBundle\Manager\SystemInformationManager;

class FreshFlashCommand extends StartUpCommand
{
    protected function configure()
    {
        $this
            ->setName('updater:freshflash')
            ->setDescription('
                This function should be called when the machine has been flashed (if the “freshflash” file exists,
                see Chapter 6) or if the data partition has been found damaged after boot.
                This function will fill the updater queue with the playlist and files which have been coupled to this machine.
            ')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (file_exists('D:\\settings\\freshflash')) {
            $host = $this->getHostServer();
            $systemInfo = new SystemInformationManager();

            $query['box']      = $systemInfo->getMacAddress();
            $query['hash']     = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);

            $client = new Client();
            $response = $client->get('https://'.$host.'/updater/freshflash.php', [], [
                'query' => $query
            ]);
            $response->send();
        }
    }
}