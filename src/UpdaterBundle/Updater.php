<?php

define('BASE_DIR', __DIR__ . '/../../');

require_once BASE_DIR.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use UpdaterBundle\Command\CheckCommand;
use UpdaterBundle\Command\RssTextCommand;
use UpdaterBundle\StartUp\PowerOnCommand;
use UpdaterBundle\StartUp\RebootCheckCommand;
use UpdaterBundle\StartUp\FreshFlashCommand;

$application = new Application();
$application->add(new CheckCommand);
$application->add(new RssTextCommand);
$application->add(new PowerOnCommand());
$application->add(new RebootCheckCommand);
$application->add(new FreshFlashCommand);
$application->run();
