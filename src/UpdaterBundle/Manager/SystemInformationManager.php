<?php

namespace UpdaterBundle\Manager;

class SystemInformationManager
{
    public function getCpuTemperature()
    {
        $json = file_get_contents('http://127.0.0.1:8085/data.json');
        $obj = json_decode($json);
        $temp = $obj->Children[0]->Children[0]->Children[0]->Children[1]->Children[0]->Value;
        $temp = preg_replace("/[^0-9]{2,}/","",$temp);

        return round($temp);
    }

    public function getFanSpeed()
    {
        $json = file_get_contents('http://127.0.0.1:8085/data.json');
        $obj = json_decode($json);
        $rpm = $obj->Children[0]->Children[0]->Children[0]->Children[2]->Children[0]->Value;
        $rpm = preg_replace("/[^0-9]/","",$rpm);

        return $rpm;
    }

    public function getIpAddress()
    {
        return trim(shell_exec(BASE_DIR.'\\bin\\ipAddress.exe'));
    }

    public function getCSQ()
    {
        if ($this->isWifiConnected()) {
            $quality = $this->getWifiSignal();
        } else {
            $quality = 30;
        };

        $CSQ = ($quality == 30 ? 'Ethernet' : 'WiFi').',Direct,'.$quality;

        return $CSQ;
    }

    public function getMacAddress()
    {
        return trim(shell_exec(BASE_DIR.'\\bin\\macAddress.exe'));
    }

    public function getNumberOfCore()
    {
        $cores = shell_exec('wmic cpu get NumberOfCores');
        preg_match_all('/^.*$/m', $cores, $matches);
        $value =  trim($matches[0][1]);

        return $value;
    }

    public function getCpuType()
    {
        $cores = shell_exec('wmic cpu get Name');
        preg_match_all('/^.*$/m', $cores, $matches);
        $value =  trim($matches[0][1]);

        return $value;
    }

    public function getGraphics()
    {
        $cores = shell_exec('wmic PATH Win32_VideoController GET Description');
        preg_match_all('/^.*$/m', $cores, $matches);
        $value =  trim($matches[0][1]);

        return $value;
    }

    public function getHdSerial()
    {
        $cores = shell_exec('wmic DISKDRIVE GET SerialNumber');
        preg_match_all('/^.*$/m', $cores, $matches);
        $value =  trim($matches[0][1]);

        return $value;
    }

    public function getHdModel()
    {
        $cores = shell_exec('wmic diskdrive get model');
        preg_match_all('/^.*$/m', $cores, $matches);
        $value =  trim($matches[0][1]);

        return $value;
    }

    public function getMotherBoardModel()
    {
        $cores = shell_exec('wmic baseboard get product');
        preg_match_all('/^.*$/m', $cores, $matches);
        $value =  trim($matches[0][1]);

        return $value;
    }

    public function getMemorySize()
    {
        $cores = shell_exec('wmic memorychip get capacity');
        preg_match_all('/^(\d+)\s+$/m', $cores, $matches);
        $memory = 0;
        foreach ($matches[1] as $slot) {
            $memory += $slot;
        }

        return round($memory/1000/1000).'MB'; // to megabytes
    }

    private function isWifiConnected()
    {
        $state     = false;
        $interface = $this->getCurrentInterfaceByMac($this->getMacAddress());
        if (preg_match('/Wireless/', $interface)) {
            $state = true;
        }

        return $state;
    }

    private function getWifiSignal()
    {
        $interface = shell_exec('netsh wlan show interfaces');
        $quality = 0;
        if (preg_match('/Signal\s*:\s*(\d+)/', $interface, $signal)) {
            $quality = round(($signal[1]*30)/100); //
        }

        return $quality;
    }


    private function getCurrentInterfaceByMac($macAddress)
    {
        return trim(shell_exec(BASE_DIR.'\\bin\\getInterfaceTypeByMac.exe '.$macAddress));
    }
}