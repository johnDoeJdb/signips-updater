<?php

namespace UpdaterBundle\Manager;

use Symfony\Component\Yaml\Yaml;

class ParametersBag
{
    private static $parameters = null;

    public static function init()
    {
        if (!self::$parameters) {
            self::$parameters = Yaml::parse(file_get_contents('parameters.yml'));
        }
    }

    public static function getParameter($parameters)
    {
        self::init();
        $tree       = explode('.', $parameters);
        $length     = count($tree);
        $level      = 0;
        $parameter  = self::$parameters;
        while ($level < $length) {
            if ($parameter) {
                $parameter = self::getNextElement($parameter, $tree[$level]);
            } else {
                return null;
            }
            $level++;
        }

        return $parameter;
    }

    private static function getNextElement($array, $element)
    {
        foreach ($array as $key => $item) {
            if ($key == $element) {
                return $item;
            }
        }

        return null;
    }
}