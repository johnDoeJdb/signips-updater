<?php

namespace UpdaterBundle\Rss;

use Guzzle\Http\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UpdaterBundle\Helper\Helper;
use UpdaterBundle\Logger\Logger;
use UpdaterBundle\Manager\SystemInformationManager;

class RssDataDownloader
{
    use Helper;
    public function downloadRssData()
    {
        $response = $this->sendRequest();
        if ($response) {
            Logger::log('Got feed');
            preg_match_all('/^FEED.*$\n(^FILE.*$\n?)*/m', $response, $matches);
            $feeds = $matches[0];
            foreach ($feeds as $feed) {
                $this->downloadFeedData($feed);
            }

        }
    }

    private function downloadFeedData($feed)
    {
        $rows       = $this->parseFeed($feed);
        $filesNames = [];
        $feedName   = '';
        $feedPath   = '';
        foreach ($rows as $row) {
            if ($row[0] == "FEED") {
                $feedName = $row[2];
                $feedPath = $row[3];
                @mkdir($row[3]);
            } elseif ($row[0] == "FILE") {
                $filesNames[] = $fileName = $row[2];
                $md5          = $row[3];
                $filePath     = $feedPath.'\\'.$fileName;
                if (!file_exists($feedPath) || md5_file($filePath) != $md5) {
                    $file = $this->sendRequest($feedName, $fileName);
                    @file_put_contents($filePath, $file);
                }
            }
        }
        Logger::log('Downloaded feed items');
        $this->clearOldFiles($feedPath, $filesNames);
    }

    private function parseFeed($feed)
    {
        preg_match_all('/^.*$/m', $feed, $rows);
        $parsedRows = [];
        foreach ($rows[0] as $row) {
            $parsedRows[] = explode(' ', $row);
        }

        return $parsedRows;
    }

    private function sendRequest($feedName = null, $fileName = null)
    {
        $systemInfo    = new SystemInformationManager();
        $query['box']  = $systemInfo->getMacAddress();
        $query['hash'] = SHA1('92C5E2DB6F4FC765CF97B6F83E78943E316DC17C190E86B7A0BB18A60933BA1B'.$query['box']);
        if ($feedName && $fileName) {
            $query['feed']  = $feedName;
            $query['file'] = $fileName;
        }
        $client   = new Client();
        $response = $client->get('https://'.$this->getHostServer().'/updater/rssfiles.php', [], [
            'query' => $query
        ]);

        return (string) $response->getResponse()->getBody();
    }

    private function clearOldFiles($feedPath, $feedFiles)
    {
        $directoryFiles = scandir($feedPath);
        if(is_array($directoryFiles))
        {
            foreach($directoryFiles as $file)
            {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (!in_array($file, $feedFiles)) {
                    shell_exec('del '.$feedPath.'\\'.$file);
                }
            }
        }
    }
}