<?php

namespace UpdaterBundle\Helper;

trait Helper
{
    public function getHostServer()
    {
        return trim(file_get_contents('D:\\settings\\server'));
    }

    public function getBasePath()
    {
        return realpath(BASE_DIR);
    }

    public function getBinPath()
    {
        return $this->getBasePath().'\\bin';
    }

    public function getUpdateTime()
    {
        return trim(file_get_contents('D:\\settings\\updatetime'));
    }

    public function getLastUpdateId()
    {
        return trim(file_get_contents(self::getBasePath().'\\data\\lastId'));
    }

    public function setLastUpdateId($id)
    {
        return trim(file_put_contents(self::getBasePath().'\\data\\lastId', $id));
    }
}