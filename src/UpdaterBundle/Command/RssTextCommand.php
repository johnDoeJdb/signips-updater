<?php

namespace UpdaterBundle\Command;

use Guzzle\Http\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UpdaterBundle\Helper\Helper;
use UpdaterBundle\Logger\Logger;
use UpdaterBundle\Manager\SystemInformationManager;
use UpdaterBundle\Response\ResponseParser;
use UpdaterBundle\Rss\RssDataDownloader;


class RssTextCommand extends Command
{
    use Helper;
    protected function configure()
    {
        $this
            ->setName('updater:rss')
            ->setDescription('
                The RSS data interface uses standard HTTPS requests to request the RSS data updates,
                basically in the same way as the updater interface.
                It should update the text and the data files every time the updater service runs.
            ')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Logger::log('RSS updater started');
        $host = $this->getHostServer();
        $systemInfo = new SystemInformationManager();
        $query['box']     = $systemInfo->getMacAddress();
        $query['hash']    = SHA1('92C5E2DB6F4FC765CF97B6F83E78943E316DC17C190E86B7A0BB18A60933BA1B'.$query['box']);
        $client = new Client();
        $response = $client->get('https://'.$host.'/updater/rss.php', [], [
            'query' => $query
        ]);
        $response->send();
        $RssDataDownloader = new RssDataDownloader();
        $responseBody = (string) $response->getResponse()->getBody();
        if ($responseBody) {
            @mkdir('D:\\temporary');
            file_put_contents('D:\\temporary\\db_rss.xml', $responseBody);
            Logger::log('Got db_rss.xml');
            $RssDataDownloader->downloadRssData();

        }
        Logger::log('RSS updater finished');
    }
}