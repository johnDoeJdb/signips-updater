<?php

namespace UpdaterBundle\Command;

use Guzzle\Http\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UpdaterBundle\Helper\Helper;
use UpdaterBundle\Logger\Logger;
use UpdaterBundle\Manager\SystemInformationManager;
use UpdaterBundle\Response\ResponseParser;


class CheckCommand extends Command
{
    use Helper;
    protected function configure()
    {
        $this
            ->setName('updater:check')
            ->setDescription('
                The updater interface uses standard HTTPS requests to process its updates from the server.
                The updater is the main interface between the machine and the server, it’s essential that it stays working.
            ')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $this->getHostServer();
        $updateTime = $this->getUpdateTime();
        while(true) {
            Logger::log('Updater started');
            $systemInfo = new SystemInformationManager();
            $query['box']     = $systemInfo->getMacAddress();
            $query['hash']    = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);
            $query['cputemp'] = $systemInfo->getCpuTemperature();
            $query['cpufan']  = $systemInfo->getFanSpeed();
            $query['localip'] = $systemInfo->getIpAddress();
            $query['csq']     = $systemInfo->getCSQ();
            $query['ver']     = 3;

            $client = new Client();
            $response = $client->get('https://'.$host.'/updater/check.php', [], [
                    'query' => $query]
            );
            $response->send();
            Logger::log("Current machine status; temp: {$systemInfo->getCpuTemperature()}C, fanspeed: {$systemInfo->getFanSpeed()}RPM");
            $responseBody = (string) $response->getResponse()->getBody();
            ResponseParser::parseResponse($responseBody);
            Logger::log('Updater finished');
            sleep((integer)$updateTime*60);
        }

    }
}