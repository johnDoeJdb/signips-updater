<?php

namespace UpdaterBundle\Response;

use UpdaterBundle\Helper\Helper;
use UpdaterBundle\Response\Command\DeleteCommand;
use UpdaterBundle\Response\Command\DownloadCommand;
use UpdaterBundle\Response\Command\DownloadPackageCommand;
use UpdaterBundle\Response\Command\InvisibleCommand;
use UpdaterBundle\Response\Command\LogCommand;
use UpdaterBundle\Response\Command\RebootCommand;
use UpdaterBundle\Response\Command\RestartPlayerCommand;
use UpdaterBundle\Response\Command\UploadCommand;
use UpdaterBundle\Response\Command\VisibleCommand;

class ResponseCommand
{
    use Helper;

    public static function runCommand($parsedCommand)
    {
        $action = $parsedCommand[0];
        $lastId = $parsedCommand[1];
        if ((integer) $lastId !== (integer) self::getLastUpdateId()) {
            switch ($action) {
                case 'c':
                    $command = new InvisibleCommand($parsedCommand);
                    break;
                case 'v':
                    $command = new VisibleCommand($parsedCommand);
                    break;
                case 'f':
                    $command = new DownloadCommand($parsedCommand);
                    break;
                case 'h':
                    $command = new DownloadPackageCommand($parsedCommand);
                    break;
                case 'l':
                    $command = new LogCommand($parsedCommand);
                    break;
                case 'u':
                    $command = new UploadCommand($parsedCommand);
                    break;
                case 'd':
                    $command = new DeleteCommand($parsedCommand);
                    break;
                case 'r':
                    $command = new RebootCommand($parsedCommand);
                    break;
                case 'z':
                    $command = new RestartPlayerCommand($parsedCommand);
                    break;
                default:
                    throw new \Exception('Command does not find');
            }
            $command->run();
            self::setLastUpdateId($lastId);
        }
    }
}