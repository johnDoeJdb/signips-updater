<?php

namespace UpdaterBundle\Response\Command;

use Symfony\Component\Process\Process;

class InvisibleCommand extends AbstractCommand
{
    public function run()
    {
        $process = new Process('start /B /MIN /WAIT '.$this->argument);
        $process->setTimeout(3600);
        $process->run();
        $output = $process->getOutput();
        $this->done($this->updaterId, $output);
    }
}