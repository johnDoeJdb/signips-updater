<?php

namespace UpdaterBundle\Response\Command;

use Symfony\Component\Process\Process;

class VisibleCommand extends AbstractCommand
{
    public function run()
    {
        $process = new Process('start /B /MAX /WAIT '.$this->argument);
        $process->setTimeout(3600);
        $process->run();
        $output = $process->getOutput();
        $this->done($this->updaterId, $output);
    }
}