<?php

namespace UpdaterBundle\Response\Command;

class DeleteCommand extends AbstractCommand
{
    public function run()
    {
        $localPath    = $this->argument;

        if (file_exists($localPath)) {
            shell_exec('del '.$localPath);
            if (file_exists($localPath)) {
                $this->done($this->updaterId, 'File deleted successfully');
            } else {
                $this->error($this->updaterId);
            }
        } else {
            $this->done($this->updaterId, 'File already deleted');
        }
    }
}