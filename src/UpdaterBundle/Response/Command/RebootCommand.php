<?php

namespace UpdaterBundle\Response\Command;

class RebootCommand extends AbstractCommand
{
    public function run()
    {
        @mkdir('D:\\temporary');
        @touch('D:\\temporary\\reboot');
        @file_put_contents('D:\\temporary\\reboot', $this->updaterId);
        shell_exec('shutdown /r /f /t 0');
        die;
    }
}