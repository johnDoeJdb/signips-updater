<?php

namespace UpdaterBundle\Response\Command;

use UpdaterBundle\Logger\Logger;

class DownloadCommand extends AbstractCommand
{
    public function run()
    {
        $md5          = $this->command[3];
        $localPath    = $this->argument;
        $server       = $this->getHostServer();
        $fileUrl      = 'https://'.$server.'/updater/files/'.$this->updaterId;
        $counter      = 0;

        if (!file_exists($localPath) || md5_file($localPath) != $md5) {
            do {
                Logger::log('file URL: '.$fileUrl);
                Logger::log('local path: '.$localPath);
                $file = file_get_contents($fileUrl);
                if (md5($file) == $md5) {
                    file_put_contents($localPath, $file);
                    break;
                }
                $counter++;
            } while ($counter < 10);
            if ($counter < 10) {
                $speed = $this->speedTest($fileUrl).' kb/s';
                Logger::log('Speed: '.$speed);
                $this->done($this->updaterId, $speed);
            } else {
                $this->error($this->updaterId);
            }
        } else {
            $this->done($this->updaterId, "File already existed");
        }
    }

    private function speedTest($fileUrl)
    {
        $start = time(true);
        $fileSize = '10240'; // if the file's size is 10MB
        for ($i=0; $i<10; $i++) {
            file_get_contents($fileUrl);
        }
        $end = time(true);
        $speed = ($fileSize / ($end - $start)) / $i * 8;

        return $speed; // will return the speed in kbps
    }
}