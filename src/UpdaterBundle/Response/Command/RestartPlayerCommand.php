<?php

namespace UpdaterBundle\Response\Command;

use UpdaterBundle\Manager\ParametersBag;

class RestartPlayerCommand extends AbstractCommand
{
    public function run()
    {
        $playerPath = ParametersBag::getParameter('player_path');
        shell_exec('taskkill /im chrome.exe /f');
        shell_exec($playerPath);
        $this->done($this->updaterId, 'Player restarted successfully');
    }
}