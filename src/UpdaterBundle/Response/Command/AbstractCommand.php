<?php

namespace UpdaterBundle\Response\Command;

use UpdaterBundle\Helper\Helper;
use Guzzle\Http\Client;
use UpdaterBundle\Logger\Logger;
use UpdaterBundle\Manager\SystemInformationManager;

abstract class AbstractCommand
{
    use Helper;
    protected $command;
    protected $argument;
    protected $commandAlias;

    public function __construct($command)
    {
        $this->commandAlias  = $command[0];
        $this->command       = $command;
        $this->updaterId     = $command[1];
        $this->argument      = base64_decode($command[2]);
    }

    abstract public function run();

    public static function done($updaterId, $message)
    {
        $host = self::getHostServer();
        $client = new Client();
        $systemInfo = new SystemInformationManager();
        $query['box']  = $systemInfo->getMacAddress();
        $query['hash'] = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);
        $query['id']   = $updaterId;
        $query['log']  = $message;

        $response = $client->get('https://'.$host.'/updater/okay.php', [], [
            'query' => $query
        ]);
        $response->send();
        Logger::log('"done" function was called');
    }

    public static function error($updaterId)
    {
        $host = self::getHostServer();
        $client = new Client();
        $systemInfo = new SystemInformationManager();
        $query['box']  = $systemInfo->getMacAddress();
        $query['hash'] = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);
        $query['id']   = $updaterId;

        $response = $client->get('https://'.$host.'/updater/error.php', [], [
            'query' => $query
        ]);
        $response->send();
        Logger::log('"error" function was called');
    }
}