<?php

namespace UpdaterBundle\Response\Command;

use Guzzle\Http\Client;
use UpdaterBundle\Manager\SystemInformationManager;

class UploadCommand extends AbstractCommand
{
    public function run()
    {
        $host = $this->getHostServer();
        $client = new Client();
        $systemInfo = new SystemInformationManager();
        $query['box']  = $systemInfo->getMacAddress();
        $query['hash'] = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);

        if (file_exists($this->argument)) {
            $response = $client->post('https://'.$host.'/upload.php', [], [
                'query' => $query,
                'body' => [
                    'uploadfile' => fopen($this->argument, 'r'),
                    'filename' => $this->argument,
                ]
            ]);
            $response->send();
            $this->done($this->updaterId, "File uploaded successfully");
        } else {
            $this->error($this->updaterId);
        }
    }
}