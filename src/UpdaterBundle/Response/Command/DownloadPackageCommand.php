<?php

namespace UpdaterBundle\Response\Command;

class DownloadPackageCommand extends AbstractCommand
{
    public function run()
    {
        $md5           = $this->command[3];
        $localPath     = $this->argument;
        $server        = $this->getHostServer();
        $fileUrl       = 'https://'.$server.'/updater/files/'.$this->updaterId;
        $zipArchivator = $this->getBinPath().'\\7za.exe';
        $counter       = 0;

        if (!file_exists($localPath) || !file_exists($localPath.'\\template.zip') || md5_file($localPath.'\\template.zip') != $md5) {
            do {
                $file = @file_get_contents($fileUrl);
                if (md5_file($file) == $md5) {
                    @file_put_contents($localPath.'\\template.zip', $file);
                    shell_exec($zipArchivator.' x '.$localPath.'\\template.zip -o'.$localPath);
                    break;
                }
                $counter++;
            } while ($counter < 10);
            if ($counter < 10) {
                $this->done($this->updaterId, $this->speedTest($fileUrl).' kb/s');
            } else {
                $this->error($this->updaterId);
            }
        } else {
            $this-> done($this->updaterId, "File already existed");
        }
    }

    private function speedTest($fileUrl)
    {
        $start = time(true);
        $fileSize = '10240'; // if the file's size is 10MB
        for ($i=0; $i<10; $i++) {
            file_get_contents($fileUrl);
        }
        $end = time(true);
        $speed = ($fileSize / ($end - $start)) / $i * 8;

            return $speed; // will return the speed in kbps
    }
}