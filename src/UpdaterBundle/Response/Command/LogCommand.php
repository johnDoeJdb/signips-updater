<?php

namespace UpdaterBundle\Response\Command;

use Guzzle\Http\Client;
use UpdaterBundle\Manager\SystemInformationManager;

class LogCommand extends AbstractCommand
{
    public function run()
    {
        $date          = new \DateTime();
        $logPath       = 'D:\\logs';
        $zipArchivator = $this->getBinPath().'\\7za.exe';
        $logs[]        = $logPath.'\\'.$date->format('Y-m-d').'.log';
        $logs[]        = $logPath.'\\'.$date->modify('-1 day')->format('Y-m-d').'.log';
        $host          = $this->getHostServer();
        $client        = new Client();
        $systemInfo    = new SystemInformationManager();
        $query['box']  = $systemInfo->getMacAddress();
        $query['hash'] = SHA1('3CCB62B3A8312BEC25FCE4457F44A5673BCB5AFC1983B3E6EF2B7249CF5AAFCD'.$query['box']);

        foreach ($logs as $key => &$file) {
            if (!file_exists($file)) {
                unset($logs[$key]);
            }
        }

        if (count($logs)) {
            shell_exec($zipArchivator.' a '.$logPath.'\\logfile.zip '.implode(' ', $logs));
            $response = $client->post('https://'.$host.'/updater/logs.php', [], [
                'query' => $query,
                'body' => [
                    'logfile' => fopen($logPath.'\\logfile.zip', 'r'),
                ]
            ]);
            $response = $response->send();
            if ($response->getStatusCode() == 200) {
                $this->done($this->updaterId, "Logs sent successfully");
            } else {
                $this->error($this->updaterId);
            };
        } else {
            $this->error($this->updaterId);
        }

    }
}