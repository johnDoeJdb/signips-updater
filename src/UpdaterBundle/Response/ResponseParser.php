<?php

namespace UpdaterBundle\Response;

use UpdaterBundle\Logger\Logger;

class ResponseParser
{
    public static function parseResponse($response)
    {
        if ($response == '-1' || $response == '0') {
            if ($response == '-1') {
                Logger::log('Machine is not yet added to the CMS');
            } else {
                Logger::log('Has no updates in its queue');
            }

            return 0;
        } elseif (strlen($response) > 3) {
            Logger::log('Got command: '.$response);
            $command = explode(' ', $response);
            ResponseCommand::runCommand($command);
        }
    }
}