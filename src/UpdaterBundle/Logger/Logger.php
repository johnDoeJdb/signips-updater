<?php

namespace UpdaterBundle\Logger;

class Logger
{
    public static function log($message)
    {
        $logFile = 'D:\\logs\\'.date('Y-m-d').'.log';
        $time    = date('H:i:s');
        if (!file_exists($logFile)) {
            touch($logFile);
        }
        $row = "[{$time}] {$message} \r\n";
        file_put_contents($logFile, $row, FILE_APPEND);
    }
}